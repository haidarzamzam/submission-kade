package com.haidev.footballmatchapp

import android.view.KeyEvent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.haidev.footballmatchapp.main.views.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testRecyclerViewBehaviour() {
        onView(withId(R.id.rvLeague))
            .check(matches(isDisplayed()))
        Thread.sleep(5000)
        onView(withId(R.id.rvLeague)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                5
            )
        )
        onView(withId(R.id.rvLeague)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click())
        )

        onView(withId(R.id.action_search))
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_search)).perform(click())

        onView(withId(R.id.search_src_text)).perform(
            typeText("ever"),
            pressKey(KeyEvent.KEYCODE_ENTER)
        )

        Thread.sleep(5000)
        onView(withId(R.id.rvSearchMatch))
            .check(matches(isDisplayed()))
        onView(withId(R.id.rvSearchMatch)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click())
        )
        Thread.sleep(1000)
    }

}