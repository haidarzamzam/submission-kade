package com.haidev.footballmatchapp.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(
    ctx, "db_favorite",
    null, 3
) {

    companion object {

        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }

    }


    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            FavMatchModel.TABLE_FAVORITE, true,
            FavMatchModel.ID_FAVORITE to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavMatchModel.TITLE_FAVORITE to TEXT,
            FavMatchModel.DATE_FAVORITE to TEXT,
            FavMatchModel.DESC_FAVORITE to TEXT,
            FavMatchModel.SCORE_AWAY to TEXT,
            FavMatchModel.SCORE_HOME to TEXT,
            FavMatchModel.GOALS_AWAY to TEXT,
            FavMatchModel.GOALS_HOME to TEXT,
            FavMatchModel.RED_CARD_AWAY to TEXT,
            FavMatchModel.RED_CARD_HOME to TEXT,
            FavMatchModel.YELLOW_CARD_AWAY to TEXT,
            FavMatchModel.YELLOW_CARD_HOME to TEXT,
            FavMatchModel.ROLE_FAVORITE to TEXT
        )

        db?.createTable(
            FavTeamModel.TABLE_TEAM, true,
            FavTeamModel.ID_TEAM to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavTeamModel.TITLE_TEAM to TEXT,
            FavTeamModel.DESC_TEAM to TEXT,
            FavTeamModel.LEAGUE_TEAM to TEXT,
            FavTeamModel.IMAGE_TEAM to TEXT,
            FavTeamModel.IMAGE_STADIUM to TEXT,
            FavTeamModel.IMAGE_JERSEY to TEXT,
            FavTeamModel.NAME_STADIUM to TEXT
        )

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // menghapus table
        db?.dropTable(FavMatchModel.TABLE_FAVORITE, true)
        db?.dropTable(FavTeamModel.TABLE_TEAM, true)
    }
}

// extension function
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)