package com.haidev.footballmatchapp.network.repositories

import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class LeaguesRepository(val schedulerProvider: SchedulerProvider) {
    private val compositeDisposable = CompositeDisposable()
    private val apiService = ServiceFactory.create()


    fun cleared() {
        compositeDisposable.clear()
    }

}