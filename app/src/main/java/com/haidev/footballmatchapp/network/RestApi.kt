package com.haidev.footballmatchapp.network

import com.haidev.footballmatchapp.menu.league.models.DetailLeagueModel
import com.haidev.footballmatchapp.menu.league.models.LeaguesModel
import com.haidev.footballmatchapp.menu.match.models.MatchModel
import com.haidev.footballmatchapp.menu.match.models.SearchMatchModel
import com.haidev.footballmatchapp.menu.match.models.StandingModel
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {
    @GET("search_all_leagues.php?s=Soccer")
    fun getListLeagues(): Observable<LeaguesModel>

    @GET("lookupleague.php?")
    fun getDetailLeague(
        @Query("id") id: String?
    ): Observable<DetailLeagueModel>

    @GET("eventspastleague.php?")
    fun getLastMatch(
        @Query("id") id: String?
    ): Observable<MatchModel>

    @GET("lookupteam.php?")
    fun getDetailTeam(
        @Query("id") id: String?
    ): Observable<TeamModel>

    @GET("eventsnextleague.php?")
    fun getNextMatch(
        @Query("id") id: String?
    ): Observable<MatchModel>

    @GET("lookupevent.php?")
    fun getDetailMatch(
        @Query("id") id: String?
    ): Observable<MatchModel>

    @GET("searchevents.php?")
    fun getSearchMatch(
        @Query("e") e: String?
    ): Observable<SearchMatchModel>

    @GET("lookuptable.php?")
    fun getStanding(
        @Query("l") id: String?
    ): Observable<StandingModel>

    @GET("lookup_all_teams.php?")
    fun getTeam(
        @Query("id") id: String?
    ): Observable<TeamModel>

    @GET("searchteams.php?")
    fun getSearchTeam(
        @Query("t") e: String?
    ): Observable<TeamModel>
}