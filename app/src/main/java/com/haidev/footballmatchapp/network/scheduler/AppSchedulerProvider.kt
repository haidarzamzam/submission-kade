package com.haidev.footballmatchapp.network.scheduler

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AppSchedulerProvider : SchedulerProvider {
    override fun io() = Schedulers.io()
    override fun computation() = Schedulers.computation()
    override fun mainThread() = AndroidSchedulers.mainThread()

}