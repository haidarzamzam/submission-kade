package com.haidev.footballmatchapp.menu.favorite.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListFavTeamBinding
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.ItemFavTeamViewModel

class ItemFavTeamAdapter(
    private val context: Context,
    private var listFavTeam: MutableList<FavTeamModel>
) :
    RecyclerView.Adapter<ItemFavTeamAdapter.ItemFavTeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemFavTeamViewHolder {
        val binding: ItemListFavTeamBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_fav_team,
                parent,
                false
            )
        return ItemFavTeamViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavTeam.size
    }

    override fun onBindViewHolder(holder: ItemFavTeamViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(listFavTeam[fixPosition], context)
    }

    class ItemFavTeamViewHolder(val binding: ItemListFavTeamBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemFavTeamViewModel

        fun bindBinding(model: FavTeamModel, context: Context) {
            viewModel = ItemFavTeamViewModel(context, model, binding)
            binding.itemFavTeam = viewModel
            binding.executePendingBindings()
        }

    }
}