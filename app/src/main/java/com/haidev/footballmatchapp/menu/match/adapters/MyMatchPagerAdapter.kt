package com.haidev.footballmatchapp.menu.match.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.haidev.footballmatchapp.menu.match.views.MatchFragment
import com.haidev.footballmatchapp.menu.match.views.StandingFragment
import com.haidev.footballmatchapp.menu.match.views.TeamFragment

class MyMatchPagerAdapter(fm: FragmentManager, id: String?) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        MatchFragment(id),
        StandingFragment(id),
        TeamFragment(id)

    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }


}