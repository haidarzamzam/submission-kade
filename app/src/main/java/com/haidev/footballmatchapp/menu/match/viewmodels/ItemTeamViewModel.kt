package com.haidev.footballmatchapp.menu.match.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.databinding.ItemListTeamBinding
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.views.DetailTeamActivity
import com.squareup.picasso.Picasso

class ItemTeamViewModel(
    private val context: Context,
    val model: ResultTeam,
    val binding: ItemListTeamBinding
) {
    var name: ObservableField<String?> = ObservableField(model.strTeam)
    var desc: ObservableField<String?> = ObservableField(model.strDescriptionEN)

    init {
        Picasso.get().load(
            model.strTeamBadge
        ).into(binding.ivTeam)
    }

    fun clickDetailTeam(view: View) {
        val intent = Intent(context, DetailTeamActivity::class.java)
        intent.putExtra("putId", model.idTeam)
        intent.putExtra("putTitle", model.strTeam)
        intent.putExtra("putLeague", model.strLeague)
        intent.putExtra("putLogo", model.strTeamBadge)
        context.startActivity(intent)
    }
}