package com.haidev.footballmatchapp.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentFavTeamBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.adapters.ItemFavTeamAdapter
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.FavTeamViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavTeamFragment : Fragment() {

    private lateinit var favTeamBinding: FragmentFavTeamBinding
    private lateinit var vmFavTeam: FavTeamViewModel

    private var isExist: Boolean = false
    private lateinit var adapter: ItemFavTeamAdapter
    private var listFavTeam: MutableList<FavTeamModel> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        favTeamBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_fav_team, container, false)
        vmFavTeam = ViewModelProviders.of(this).get(FavTeamViewModel::class.java)
        favTeamBinding.favTeam = vmFavTeam

        favMatchDbState()
        setupRecycler()

        return favTeamBinding.root
    }

    override fun onResume() {
        super.onResume()
        favMatchDbState()
        setupRecycler()
    }

    private fun favMatchDbState() {
        context?.database?.use {
            val result = select(FavTeamModel.TABLE_TEAM)

            val datas = result.parseList(classParser<FavTeamModel>())
            if (datas.isNotEmpty()) isExist = true
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        favTeamBinding.rvFavTeam.layoutManager = lManager
        favTeamBinding.rvFavTeam.setHasFixedSize(true)

        adapter = ItemFavTeamAdapter(context!!, listFavTeam)

        favTeamBinding.rvFavTeam.adapter = adapter
        if (isExist) {
            favTeamBinding.txtNoDataMatch.visibility = View.INVISIBLE
            showFavorite()
        } else {
            favTeamBinding.txtNoDataMatch.visibility = View.VISIBLE
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            listFavTeam.clear()
            val result = select(FavTeamModel.TABLE_TEAM)
            val favorite = result.parseList(classParser<FavTeamModel>())
            listFavTeam.addAll(favorite)
            adapter.notifyDataSetChanged()

        }
    }

}
