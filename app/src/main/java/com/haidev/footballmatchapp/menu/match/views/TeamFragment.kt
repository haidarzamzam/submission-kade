package com.haidev.footballmatchapp.menu.match.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentTeamBinding
import com.haidev.footballmatchapp.menu.match.adapters.ItemTeamAdapter
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.menu.match.viewmodels.TeamViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.fragment_team.*

class TeamFragment(val id: String?) : Fragment() {

    private lateinit var teamBinding: FragmentTeamBinding
    private lateinit var vmTeam: TeamViewModel

    private lateinit var adapter: ItemTeamAdapter
    private var listTeam: MutableList<ResultTeam> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        teamBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_team, container, false)
        vmTeam = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        teamBinding.team = vmTeam

        vmTeam.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        vmTeam.listtStanding.observe(this, Observer {
            onListDataChange(it)
        })
        vmTeam.error.observe(this, Observer {
            handlingError(it)
        })

        vmTeam.requestTeam(id)

        setupRecycler()
        setupSwipeRefresh()

        return teamBinding.root
    }

    private fun setupSwipeRefresh() {
        teamBinding.swipe.setOnRefreshListener {
            teamBinding.swipe.isRefreshing = false
            vmTeam.requestTeam(id)
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        teamBinding.rvTeam.layoutManager = lManager
        teamBinding.rvTeam.setHasFixedSize(true)

        adapter = ItemTeamAdapter(
            context!!,
            listTeam
        )
        teamBinding.rvTeam.adapter = adapter
    }

    private fun onListDataChange(teamModel: TeamModel?) {
        listTeam.clear()
        if (teamModel?.teams == null) {
            Toast.makeText(context, "Team NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listTeam.addAll(teamModel.teams)
            rvTeam.post {
                adapter.notifyDataSetChanged()
            }
        }
        teamBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        teamBinding.progressLoading.isVisible = false
    }


}
