package com.haidev.footballmatchapp.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentFavNextBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.adapters.ItemFavNextAdapter
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.FavNextViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavNextFragment : Fragment() {

    private lateinit var favNextBinding: FragmentFavNextBinding
    private lateinit var vmFavNext: FavNextViewModel

    private var isExist: Boolean = false
    private lateinit var adapter: ItemFavNextAdapter
    private var listFavNextMatch: MutableList<FavMatchModel> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        favNextBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_fav_next, container, false)
        vmFavNext = ViewModelProviders.of(this).get(FavNextViewModel::class.java)
        favNextBinding.favNext = vmFavNext

        favMatchDbState()
        setupRecycler()

        return favNextBinding.root
    }


    override fun onResume() {
        super.onResume()
        favMatchDbState()
        setupRecycler()
    }

    private fun favMatchDbState() {
        context?.database?.use {
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ROLE_FAVORITE = {id})",
                    "id" to "next"
                )

            val datas = result.parseList(classParser<FavMatchModel>())
            if (datas.isNotEmpty()) isExist = true
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        favNextBinding.rvFavNext.layoutManager = lManager
        favNextBinding.rvFavNext.setHasFixedSize(true)

        adapter = ItemFavNextAdapter(context!!, listFavNextMatch)

        favNextBinding.rvFavNext.adapter = adapter
        if (isExist) {
            favNextBinding.txtNoDataMatch.visibility = View.INVISIBLE
            showFavorite()
        } else {
            favNextBinding.txtNoDataMatch.visibility = View.VISIBLE
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            listFavNextMatch.clear()
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ROLE_FAVORITE = {id})",
                    "id" to "next"
                )
            val favorite = result.parseList(classParser<FavMatchModel>())
            listFavNextMatch.addAll(favorite)
            adapter.notifyDataSetChanged()

        }
    }
}
