package com.haidev.footballmatchapp.menu.favorite.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.databinding.ItemListFavTeamBinding
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import com.haidev.footballmatchapp.menu.favorite.views.DetailFavTeamActivity
import com.squareup.picasso.Picasso

class ItemFavTeamViewModel(
    private val context: Context,
    var model: FavTeamModel,
    val binding: ItemListFavTeamBinding
) {
    var title: ObservableField<String?> = ObservableField(model.title)
    var desc: ObservableField<String?> = ObservableField(model.desc)

    init {
        Picasso.get().load(model.imgLogo).into(binding.ivTeam)
    }

    fun clickDetailTeam(view: View) {
        val intent = Intent(context, DetailFavTeamActivity::class.java)
        intent.putExtra(DetailFavTeamActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }
}
