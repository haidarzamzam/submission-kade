package com.haidev.footballmatchapp.menu.match.views

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivitySearchTeamBinding
import com.haidev.footballmatchapp.menu.match.adapters.ItemSearchTeamAdapter
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.menu.match.viewmodels.SearchTeamViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.activity_search_team.*

class SearchTeamActivity : AppCompatActivity() {

    private lateinit var searchTeamBinding: ActivitySearchTeamBinding
    private lateinit var vmSearchTeam: SearchTeamViewModel

    private lateinit var adapter: ItemSearchTeamAdapter
    private var listTeam: MutableList<ResultTeam> = mutableListOf()

    var query: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchTeamBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_team)
        vmSearchTeam = ViewModelProviders.of(this).get(SearchTeamViewModel::class.java)
        searchTeamBinding.searchTeam = vmSearchTeam

        query = intent.getStringExtra("putQuery")

        vmSearchTeam.listTeam.observe(this, Observer {
            onListDataChange(it)
        })
        vmSearchTeam.error.observe(this, Observer {
            handlingError(it)
        })
        vmSearchTeam.setupRepository(AppSchedulerProvider(), ServiceFactory.create())
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        vmSearchTeam.requestSearchTeam(query)
        setupRecycler()
        setupSwipeRefresh()
    }

    private fun setupSwipeRefresh() {
        searchTeamBinding.swipe.setOnRefreshListener {
            searchTeamBinding.swipe.isRefreshing = false
            vmSearchTeam.requestSearchTeam(query)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(this)
        searchTeamBinding.rvSearchTeam.layoutManager = lManager
        searchTeamBinding.rvSearchTeam.setHasFixedSize(true)

        adapter =
            ItemSearchTeamAdapter(this, listTeam)
        rvSearchTeam.adapter = adapter
    }

    private fun onListDataChange(teamModel: TeamModel?) {
        listTeam.clear()
        if (teamModel?.teams == null) {
            Toast.makeText(this, "Search Team NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listTeam.addAll(teamModel.teams.filter { it.strSport == "Soccer" })
            rvSearchTeam.post {
                adapter.notifyDataSetChanged()
            }
        }
        searchTeamBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(this, throwable?.message, Toast.LENGTH_SHORT).show()
        searchTeamBinding.progressLoading.isVisible = false
    }
}
