package com.haidev.footballmatchapp.menu.match.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentStandingBinding
import com.haidev.footballmatchapp.menu.match.adapters.ItemStandingAdapter
import com.haidev.footballmatchapp.menu.match.models.ResultStanding
import com.haidev.footballmatchapp.menu.match.models.StandingModel
import com.haidev.footballmatchapp.menu.match.viewmodels.StandingViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.fragment_standing.*


class StandingFragment(val id: String?) : Fragment() {

    private lateinit var standingBinding: FragmentStandingBinding
    private lateinit var vmStanding: StandingViewModel

    private lateinit var adapter: ItemStandingAdapter
    private var listStanding: MutableList<ResultStanding> = mutableListOf()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        standingBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_standing, container, false)
        vmStanding = ViewModelProviders.of(this).get(StandingViewModel::class.java)
        standingBinding.standing = vmStanding
        vmStanding.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        vmStanding.listtStanding.observe(this, Observer {
            onListDataChange(it)
        })
        vmStanding.error.observe(this, Observer {
            handlingError(it)
        })

        vmStanding.requestStanding(id)

        setupRecycler()
        setupSwipeRefresh()
        return standingBinding.root
    }

    private fun setupSwipeRefresh() {
        standingBinding.swipe.setOnRefreshListener {
            standingBinding.swipe.isRefreshing = false
            vmStanding.requestStanding(id)
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        standingBinding.rvStanding.layoutManager = lManager
        standingBinding.rvStanding.setHasFixedSize(true)

        adapter = ItemStandingAdapter(
            context!!,
            listStanding
        )
        standingBinding.rvStanding.adapter = adapter
    }

    private fun onListDataChange(standingModel: StandingModel?) {
        listStanding.clear()
        if (standingModel?.table == null) {
            Toast.makeText(context, "Standing NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listStanding.addAll(standingModel.table)
            rvStanding.post {
                adapter.notifyDataSetChanged()
            }
        }
        standingBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        standingBinding.progressLoading.isVisible = false
    }


}
