package com.haidev.footballmatchapp.menu.favorite.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class FavTeamModel(
    val id: Long?,
    val title: String?,
    val desc: String?,
    val league: String?,
    val imgLogo: String?,
    val imgStadium: String?,
    val imgJersey: String?,
    val nameStadium: String?


) : Parcelable {

    // variable yang akan menjadi tag
    companion object {
        const val TABLE_TEAM: String = "TABLE_TEAM"
        const val ID_TEAM: String = "ID_TEAM"
        const val TITLE_TEAM: String = "TITLE_TEAM"
        const val DESC_TEAM: String = "DESC_TEAM"
        const val LEAGUE_TEAM: String = "LEAGUE_TEAM"
        const val IMAGE_TEAM: String = "IMAGE_TEAM"
        const val IMAGE_STADIUM: String = "IMAGE_STADIUM"
        const val IMAGE_JERSEY: String = "IMAGE_JERSEY"
        const val NAME_STADIUM: String = "NAME_STADIUM"
    }
}
