package com.haidev.footballmatchapp.menu.league.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentLeagueBinding
import com.haidev.footballmatchapp.menu.league.adapters.ItemLeagueAdapter
import com.haidev.footballmatchapp.menu.league.models.Country
import com.haidev.footballmatchapp.menu.league.models.LeaguesModel
import com.haidev.footballmatchapp.menu.league.viewmodels.LeagueViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.fragment_league.*

class LeagueFragment : Fragment() {

    private lateinit var mainBinding: FragmentLeagueBinding
    private lateinit var vmLeague: LeagueViewModel

    private lateinit var adapter: ItemLeagueAdapter
    private var listLeague: MutableList<Country> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_league, container, false)
        vmLeague = ViewModelProviders.of(this).get(LeagueViewModel::class.java)
        mainBinding.main = vmLeague

        vmLeague.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        vmLeague.listLeagueResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmLeague.error.observe(this, Observer {
            handlingError(it)
        })

        setupSwipeRefresh()
        setupRecycler()

        vmLeague.requestListLeague()

        return mainBinding.root
    }

    private fun setupSwipeRefresh() {
        mainBinding.swipe.setOnRefreshListener {
            mainBinding.swipe.isRefreshing = false
            vmLeague.requestListLeague()
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        mainBinding.rvLeague.layoutManager = lManager
        mainBinding.rvLeague.setHasFixedSize(true)

        adapter = ItemLeagueAdapter(
            context!!,
            listLeague
        )
        mainBinding.rvLeague.adapter = adapter
    }

    private fun onListDataChange(leagueModel: LeaguesModel?) {
        listLeague.clear()
        listLeague.addAll(leagueModel?.countrys!!)
        rvLeague.post {
            adapter.notifyDataSetChanged()
        }
        mainBinding.progressBarLeague.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        mainBinding.progressBarLeague.isVisible = false
    }

}
