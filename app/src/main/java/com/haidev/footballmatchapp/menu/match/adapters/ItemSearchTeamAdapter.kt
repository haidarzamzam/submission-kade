package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListSearchTeamBinding
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemSearchTeamViewModel

class ItemSearchTeamAdapter(
    private val context: Context,
    private var listTeam: MutableList<ResultTeam>
) :
    RecyclerView.Adapter<ItemSearchTeamAdapter.ItemSearchTeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemSearchTeamViewHolder {
        val binding: ItemListSearchTeamBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_search_team,
                parent,
                false
            )
        return ItemSearchTeamViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listTeam.size
    }

    override fun onBindViewHolder(holder: ItemSearchTeamViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listTeam[fixPosition])
    }

    class ItemSearchTeamViewHolder(val binding: ItemListSearchTeamBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemSearchTeamViewModel

        fun bindBinding(context: Context, model: ResultTeam) {
            viewModel = ItemSearchTeamViewModel(
                context,
                model,
                binding
            )
            binding.itemSearchMatch = viewModel
            binding.executePendingBindings()
        }

    }
}