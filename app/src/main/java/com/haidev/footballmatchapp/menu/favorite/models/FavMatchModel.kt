package com.haidev.footballmatchapp.menu.favorite.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class FavMatchModel(
    val id: Long?,
    val title: String?,
    val date: String?,
    val desc: String?,
    val scoreAway: String?,
    val scoreHome: String?,
    val goalsAway: String?,
    val goalsHome: String?,
    val redCardAway: String?,
    val redCardHome: String?,
    val yellowCardAway: String?,
    val yellowCardHome: String?,
    val role: String?

) : Parcelable {

    // variable yang akan menjadi tag
    companion object {
        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID_FAVORITE: String = "ID_FAVORITE"
        const val TITLE_FAVORITE: String = "TITLE_FAVORITE"
        const val DATE_FAVORITE: String = "DATE_FAVORITE"
        const val DESC_FAVORITE: String = "DESC_FAVORITE"
        const val SCORE_AWAY: String = "SCORE_AWAY_FAVORITE"
        const val SCORE_HOME: String = "SCORE_HOME_FAVORITE"
        const val GOALS_AWAY: String = "GOALS_AWAY_FAVORITE"
        const val GOALS_HOME: String = "GOALS_HOME_FAVORITE"
        const val RED_CARD_AWAY: String = "RED_CARD_AWAY_FAVORITE"
        const val RED_CARD_HOME: String = "RED_CARD_HOME_FAVORITE"
        const val YELLOW_CARD_AWAY: String = "YELLOW_CARD_AWAY_FAVORITE"
        const val YELLOW_CARD_HOME: String = "YELLOW_CARD_HOME_FAVORITE"
        const val ROLE_FAVORITE: String = "ROLE_FAVORITE"
    }
}
