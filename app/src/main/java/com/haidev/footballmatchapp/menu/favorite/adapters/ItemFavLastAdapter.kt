package com.haidev.footballmatchapp.menu.favorite.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListFavLastMatchBinding
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.ItemFavLastViewModel

class ItemFavLastAdapter(
    private val context: Context,
    private var listFavMovie: MutableList<FavMatchModel>
) :
    RecyclerView.Adapter<ItemFavLastAdapter.ItemFavLastViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemFavLastViewHolder {
        val binding: ItemListFavLastMatchBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_fav_last_match,
                parent,
                false
            )
        return ItemFavLastViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavMovie.size
    }

    override fun onBindViewHolder(holder: ItemFavLastViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(listFavMovie[fixPosition], context)
    }

    class ItemFavLastViewHolder(val binding: ItemListFavLastMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemFavLastViewModel

        fun bindBinding(model: FavMatchModel, context: Context) {
            viewModel = ItemFavLastViewModel(model, context)
            binding.itemFavLast = viewModel
            binding.executePendingBindings()
        }

    }
}