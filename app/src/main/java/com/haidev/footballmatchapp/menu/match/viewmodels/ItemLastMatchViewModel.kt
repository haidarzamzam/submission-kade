package com.haidev.footballmatchapp.menu.match.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.menu.match.models.ResultMatch
import com.haidev.footballmatchapp.menu.match.views.DetailMatchActivity

class ItemLastMatchViewModel(
    private val context: Context,
    val model: ResultMatch
) {

    var title: ObservableField<String?> = ObservableField(model.strEvent)
    var date: ObservableField<String?> = ObservableField(model.strDate)
    var scoreAway: ObservableField<String?> = ObservableField(model.intAwayScore)
    var scoreHome: ObservableField<String?> = ObservableField(model.intHomeScore)

    fun clickDetailMatch(view: View) {
        val intent = Intent(context, DetailMatchActivity::class.java)
        intent.putExtra("putIdEvent", model.idEvent)
        intent.putExtra("putTitleEvent", model.strEvent)
        intent.putExtra("putRole", "last")
        context.startActivity(intent)
    }
}