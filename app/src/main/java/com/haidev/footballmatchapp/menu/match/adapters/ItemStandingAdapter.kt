package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListStandingBinding
import com.haidev.footballmatchapp.menu.match.models.ResultStanding
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemStandingViewModel

class ItemStandingAdapter(
    private val context: Context,
    private var listStanding: MutableList<ResultStanding>
) :
    RecyclerView.Adapter<ItemStandingAdapter.ItemStandingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemStandingViewHolder {
        val binding: ItemListStandingBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_standing,
                parent,
                false
            )
        return ItemStandingViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listStanding.size
    }

    override fun onBindViewHolder(holder: ItemStandingViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listStanding[fixPosition])
    }

    class ItemStandingViewHolder(val binding: ItemListStandingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemStandingViewModel

        fun bindBinding(context: Context, model: ResultStanding) {
            viewModel = ItemStandingViewModel(
                context,
                model
            )
            binding.itemStanding = viewModel
            binding.executePendingBindings()
        }

    }
}