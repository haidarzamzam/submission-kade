package com.haidev.footballmatchapp.menu.match.views

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivityDetailMatchBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.match.models.MatchModel
import com.haidev.footballmatchapp.menu.match.models.ResultMatch
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.menu.match.viewmodels.DetailMatchViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import com.squareup.picasso.Picasso
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select


class DetailMatchActivity : AppCompatActivity() {

    private lateinit var detailMathBinding: ActivityDetailMatchBinding
    private lateinit var vmDetailMatch: DetailMatchViewModel
    var idEvent: String = ""
    var role: String = ""
    private var isLoaded: Boolean = false
    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null
    private var listMatch: MutableList<ResultMatch> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailMathBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_match)
        vmDetailMatch = ViewModelProviders.of(this).get(DetailMatchViewModel::class.java)
        detailMathBinding.detailMatch = vmDetailMatch

        vmDetailMatch.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        idEvent = intent.getStringExtra("putIdEvent")
        val nameEvent = intent.getStringExtra("putTitleEvent")
        role = intent.getStringExtra("putRole")

        vmDetailMatch.listDetailMatchResponse.observe(this, Observer {
            onListDataChangeMatch(it)
        })

        vmDetailMatch.listTeamAwayResponse.observe(this, Observer {
            onListDataChangeTeamAway(it)
        })

        vmDetailMatch.listTeamHomeResponse.observe(this, Observer {
            onListDataChangeTeamHome(it)
        })

        vmDetailMatch.error.observe(this, Observer {
            handlingError(it)
        })

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = nameEvent

        vmDetailMatch.requestDetailMatch(idEvent)
        favMovieDbState()
    }


    private fun favMovieDbState() {

        database.use {
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ID_FAVORITE = {id})",
                    "id" to idEvent
                )
            val favorite = result.parseList(classParser<FavMatchModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isLoaded) {
                if (isFavorite) removeFromFavMovieDb() else addToFavMovieDb()

                isFavorite = !isFavorite
                setFavMovieDb()
            } else {
                Toast.makeText(this, "Wait Data Loaded", Toast.LENGTH_SHORT).show()
            }


            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addToFavMovieDb() {
        try {
            database.use {
                insert(
                    FavMatchModel.TABLE_FAVORITE,
                    FavMatchModel.ID_FAVORITE to listMatch[0].idEvent,
                    FavMatchModel.TITLE_FAVORITE to listMatch[0].strEvent,
                    FavMatchModel.DATE_FAVORITE to listMatch[0].dateEvent,
                    FavMatchModel.DESC_FAVORITE to listMatch[0].strDescriptionEN,
                    FavMatchModel.SCORE_AWAY to listMatch[0].intAwayScore,
                    FavMatchModel.SCORE_HOME to listMatch[0].intHomeScore,
                    FavMatchModel.GOALS_AWAY to listMatch[0].strAwayGoalDetails,
                    FavMatchModel.GOALS_HOME to listMatch[0].strHomeGoalDetails,
                    FavMatchModel.RED_CARD_AWAY to listMatch[0].strAwayRedCards,
                    FavMatchModel.RED_CARD_HOME to listMatch[0].strHomeRedCards,
                    FavMatchModel.YELLOW_CARD_AWAY to listMatch[0].strAwayYellowCards,
                    FavMatchModel.YELLOW_CARD_HOME to listMatch[0].strHomeYellowCards,
                    FavMatchModel.ROLE_FAVORITE to role
                )

            }

            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavMovieDb() {
        try {
            database.use {
                delete(
                    FavMatchModel.TABLE_FAVORITE,
                    "(ID_FAVORITE = {id})",
                    "id" to idEvent
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavMovieDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }


    private fun onListDataChangeTeamAway(teamModel: TeamModel) {
        Picasso.get().load(teamModel.teams[0].strTeamBadge).into(detailMathBinding.imgAwayLogo)
    }

    private fun onListDataChangeTeamHome(teamModel: TeamModel) {
        Picasso.get().load(teamModel.teams[0].strTeamBadge).into(detailMathBinding.imgHomeLogo)
    }

    private fun onListDataChangeMatch(matchModel: MatchModel) {
        if (matchModel.events == null) {
            Toast.makeText(this, "Detail Match NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            isLoaded = true
            listMatch.addAll(matchModel.events)
            detailMathBinding.txtTitle.text = matchModel.events[0].strEvent
            detailMathBinding.txtDate.text = matchModel.events[0].dateEvent
            detailMathBinding.txtDesc.text = matchModel.events[0].strDescriptionEN
            detailMathBinding.txtScoreAway.text = matchModel.events[0].intAwayScore
            detailMathBinding.txtScoreHome.text = matchModel.events[0].intHomeScore
            detailMathBinding.txtHomeGoals.text = matchModel.events[0].strHomeGoalDetails
            detailMathBinding.txtAwayGoals.text = matchModel.events[0].strAwayGoalDetails
            detailMathBinding.txtRedCardAway.text = matchModel.events[0].strAwayRedCards
            detailMathBinding.txtRedCardHome.text = matchModel.events[0].strHomeRedCards
            detailMathBinding.txtYellowCardAways.text = matchModel.events[0].strAwayYellowCards
            detailMathBinding.txtYellowCardHome.text = matchModel.events[0].strHomeYellowCards
            detailMathBinding.progressLoading.isVisible = false

            vmDetailMatch.requestDetailTeamAway(matchModel.events[0].idAwayTeam)
            vmDetailMatch.requestDetailTeamHome(matchModel.events[0].idHomeTeam)
        }
    }

    private fun handlingError(throwable: Throwable?) {
        isLoaded = false
        Toast.makeText(this, throwable?.message, Toast.LENGTH_SHORT).show()
        detailMathBinding.progressLoading.isVisible = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.action_fav, menu)
        menuItem = menu
        setFavMovieDb()
        return true
    }
}
