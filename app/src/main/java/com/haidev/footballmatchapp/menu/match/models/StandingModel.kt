package com.haidev.footballmatchapp.menu.match.models

import com.google.gson.annotations.SerializedName


data class StandingModel(
    @SerializedName("table")
    val table: List<ResultStanding>
)

data class ResultStanding(
    @SerializedName("draw")
    val draw: String,
    @SerializedName("goalsagainst")
    val goalsagainst: Int,
    @SerializedName("goalsdifference")
    val goalsdifference: String,
    @SerializedName("goalsfor")
    val goalsfor: String,
    @SerializedName("loss")
    val loss: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("played")
    val played: String,
    @SerializedName("teamid")
    val teamid: String,
    @SerializedName("total")
    val total: String,
    @SerializedName("win")
    val win: String
)