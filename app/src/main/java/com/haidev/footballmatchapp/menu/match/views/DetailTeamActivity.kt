package com.haidev.footballmatchapp.menu.match.views

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivityDetailTeamBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.menu.match.viewmodels.DetailTeamViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import com.squareup.picasso.Picasso
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailTeamActivity : AppCompatActivity() {

    private lateinit var detailTeamBinding: ActivityDetailTeamBinding
    private lateinit var vmDetailTeam: DetailTeamViewModel
    private var isLoaded: Boolean = false
    var id: String = ""
    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null
    private var listTeam: MutableList<ResultTeam> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailTeamBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_team)
        vmDetailTeam = ViewModelProviders.of(this).get(DetailTeamViewModel::class.java)
        detailTeamBinding.detailTeam = vmDetailTeam

        id = intent.getStringExtra("putId")
        val title: String? = intent.getStringExtra("putTitle")
        vmDetailTeam.setupRepository(AppSchedulerProvider(), ServiceFactory.create())
        vmDetailTeam.requestDetailTeam(id)

        vmDetailTeam.listDetailTeamResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmDetailTeam.error.observe(this, Observer {
            handlingError(it)
        })

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title
        favTeamDbState()
    }


    private fun favTeamDbState() {
        database.use {
            val result = select(FavTeamModel.TABLE_TEAM)
                .whereArgs(
                    "(ID_TEAM = {id})",
                    "id" to id
                )

            val favorite = result.parseList(classParser<FavTeamModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }

    }


    private fun onListDataChange(teamModel: TeamModel) {
        isLoaded = true
        listTeam.addAll(teamModel.teams)
        detailTeamBinding.tvLeague.text = teamModel.teams[0].strLeague
        detailTeamBinding.txtTitle.text = teamModel.teams[0].strTeam
        detailTeamBinding.tvDesc.text = teamModel.teams[0].strDescriptionEN
        detailTeamBinding.tvStadium.text = teamModel.teams[0].strStadium
        Picasso.get().load(teamModel.teams[0].strTeamBadge).into(detailTeamBinding.imgLogo)
        Picasso.get().load(teamModel.teams[0].strTeamJersey).into(detailTeamBinding.ivJersey)
        Picasso.get().load(teamModel.teams[0].strStadiumThumb).into(detailTeamBinding.ivStadium)

        detailTeamBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        isLoaded = false
        Toast.makeText(this, throwable?.message, Toast.LENGTH_SHORT).show()

        detailTeamBinding.progressLoading.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isLoaded) {
                if (isFavorite) removeFromFavMovieDb() else addToFavMovieDb()

                isFavorite = !isFavorite
                setFavMovieDb()
            } else {
                Toast.makeText(this, "Wait Data Loaded", Toast.LENGTH_SHORT).show()
            }


            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addToFavMovieDb() {
        try {
            database.use {
                insert(
                    FavTeamModel.TABLE_TEAM,
                    FavTeamModel.ID_TEAM to id,
                    FavTeamModel.TITLE_TEAM to listTeam[0].strTeam,
                    FavTeamModel.DESC_TEAM to listTeam[0].strDescriptionEN,
                    FavTeamModel.LEAGUE_TEAM to listTeam[0].strLeague,
                    FavTeamModel.IMAGE_TEAM to listTeam[0].strTeamBadge,
                    FavTeamModel.IMAGE_STADIUM to listTeam[0].strStadiumThumb,
                    FavTeamModel.IMAGE_JERSEY to listTeam[0].strTeamJersey,
                    FavTeamModel.NAME_STADIUM to listTeam[0].strStadium
                )

            }

            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavMovieDb() {
        try {
            database.use {
                delete(
                    FavTeamModel.TABLE_TEAM,
                    "(ID_TEAM = {id})",
                    "id" to id
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.action_search, menu)
        menuInflater.inflate(R.menu.action_fav, menu)
        menuItem = menu
        setFavMovieDb()

        val searchView = menu.findItem(R.id.action_search)?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                val intent = Intent(this@DetailTeamActivity, SearchTeamActivity::class.java)
                intent.putExtra("putQuery", query)
                startActivity(intent)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })


        return super.onCreateOptionsMenu(menu)
    }

    private fun setFavMovieDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }

}
