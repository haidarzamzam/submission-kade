package com.haidev.footballmatchapp.menu.favorite.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListFavNextMatchBinding
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.ItemFavNextViewModel

class ItemFavNextAdapter(
    private val context: Context,
    private var listFavMovie: MutableList<FavMatchModel>
) :
    RecyclerView.Adapter<ItemFavNextAdapter.ItemFavNextViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemFavNextViewHolder {
        val binding: ItemListFavNextMatchBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_fav_next_match,
                parent,
                false
            )
        return ItemFavNextViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listFavMovie.size
    }

    override fun onBindViewHolder(holder: ItemFavNextViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(listFavMovie[fixPosition], context)
    }

    class ItemFavNextViewHolder(val binding: ItemListFavNextMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemFavNextViewModel

        fun bindBinding(model: FavMatchModel, context: Context) {
            viewModel = ItemFavNextViewModel(model, context)
            binding.itemFavNext = viewModel
            binding.executePendingBindings()
        }

    }
}