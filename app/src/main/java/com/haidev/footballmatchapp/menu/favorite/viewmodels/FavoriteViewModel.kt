package com.haidev.footballmatchapp.menu.favorite.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class FavoriteViewModel(application: Application) : AndroidViewModel(application)