package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListNextMatchBinding
import com.haidev.footballmatchapp.menu.match.models.ResultMatch
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemNextMatchViewModel

class ItemNextMatchAdapter(
    private val context: Context,
    private var listMovie: MutableList<ResultMatch>
) :
    RecyclerView.Adapter<ItemNextMatchAdapter.ItemNextMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemNextMatchViewHolder {
        val binding: ItemListNextMatchBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_next_match,
                parent,
                false
            )
        return ItemNextMatchViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listMovie.size
    }

    override fun onBindViewHolder(holder: ItemNextMatchViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listMovie[fixPosition])
    }

    class ItemNextMatchViewHolder(val binding: ItemListNextMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemNextMatchViewModel

        fun bindBinding(context: Context, model: ResultMatch) {
            viewModel = ItemNextMatchViewModel(
                context,
                model
            )
            binding.itemNextMatch = viewModel
            binding.executePendingBindings()
        }

    }
}