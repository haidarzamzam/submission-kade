package com.haidev.footballmatchapp.menu.favorite.views

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivityDetailFavTeamBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.models.FavTeamModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.DetailFavTeamViewModel
import com.squareup.picasso.Picasso
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailFavTeamActivity : AppCompatActivity() {
    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    private lateinit var detailFavTeamBinding: ActivityDetailFavTeamBinding
    private lateinit var vmDetailTeamFav: DetailFavTeamViewModel

    private lateinit var listFavTeamModel: FavTeamModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailFavTeamBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_detail_fav_team)
        vmDetailTeamFav = ViewModelProviders.of(this).get(DetailFavTeamViewModel::class.java)
        detailFavTeamBinding.detailFavTeam = vmDetailTeamFav

        listFavTeamModel = intent.getParcelableExtra(DetailFavActivity.EXTRA_DATA_LIST)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = listFavTeamModel.title

        detailFavTeamBinding.txtTitle.text = listFavTeamModel.title
        detailFavTeamBinding.tvDesc.text = listFavTeamModel.desc
        detailFavTeamBinding.tvLeague.text = listFavTeamModel.league
        detailFavTeamBinding.tvStadium.text = listFavTeamModel.nameStadium
        Picasso.get().load(listFavTeamModel.imgLogo).into(detailFavTeamBinding.imgLogo)
        Picasso.get().load(listFavTeamModel.imgJersey).into(detailFavTeamBinding.ivJersey)
        Picasso.get().load(listFavTeamModel.imgStadium).into(detailFavTeamBinding.ivStadium)

        favMovieDbState()
    }

    private fun favMovieDbState() {
        database.use {
            val result = select(FavTeamModel.TABLE_TEAM)
                .whereArgs(
                    "(ID_TEAM = {id})",
                    "id" to listFavTeamModel.id.toString()
                )
            val favorite = result.parseList(classParser<FavTeamModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isFavorite) removeFromFavMovieDb() else addToFavMovieDb()

            isFavorite = !isFavorite
            setFavMovieDb()

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addToFavMovieDb() {
        try {
            database.use {
                insert(
                    FavTeamModel.TABLE_TEAM,
                    FavTeamModel.ID_TEAM to listFavTeamModel.id,
                    FavTeamModel.TITLE_TEAM to listFavTeamModel.title,
                    FavTeamModel.DESC_TEAM to listFavTeamModel.desc,
                    FavTeamModel.LEAGUE_TEAM to listFavTeamModel.league,
                    FavTeamModel.IMAGE_TEAM to listFavTeamModel.imgLogo,
                    FavTeamModel.IMAGE_STADIUM to listFavTeamModel.imgStadium,
                    FavTeamModel.IMAGE_JERSEY to listFavTeamModel.imgJersey,
                    FavTeamModel.NAME_STADIUM to listFavTeamModel.nameStadium
                )

            }

            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavMovieDb() {
        try {

            database.use {
                delete(
                    FavTeamModel.TABLE_TEAM,
                    "(ID_TEAM = {id})",
                    "id" to listFavTeamModel.id.toString()
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavMovieDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.action_fav, menu)
        menuItem = menu
        setFavMovieDb()
        return true
    }
}
