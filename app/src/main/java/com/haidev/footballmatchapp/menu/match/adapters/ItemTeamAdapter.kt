package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListTeamBinding
import com.haidev.footballmatchapp.menu.match.models.ResultTeam
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemTeamViewModel

class ItemTeamAdapter(
    private val context: Context,
    private var listTeam: MutableList<ResultTeam>
) :
    RecyclerView.Adapter<ItemTeamAdapter.ItemTeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemTeamViewHolder {
        val binding: ItemListTeamBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_team,
                parent,
                false
            )
        return ItemTeamViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listTeam.size
    }

    override fun onBindViewHolder(holder: ItemTeamViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listTeam[fixPosition])
    }

    class ItemTeamViewHolder(val binding: ItemListTeamBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemTeamViewModel

        fun bindBinding(context: Context, model: ResultTeam) {
            viewModel = ItemTeamViewModel(
                context,
                model,
                binding
            )
            binding.itemTeam = viewModel
            binding.executePendingBindings()
        }

    }
}