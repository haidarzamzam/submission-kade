package com.haidev.footballmatchapp.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentFavoriteBinding
import com.haidev.footballmatchapp.menu.favorite.adapters.MyFavPagerAdapter
import com.haidev.footballmatchapp.menu.favorite.viewmodels.FavoriteViewModel

class FavoriteFragment : Fragment() {

    private lateinit var favBinding: FragmentFavoriteBinding
    private lateinit var vmFav: FavoriteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)
        vmFav = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)
        favBinding.fav = vmFav

        favBinding.viewpagerMain.adapter =
            MyFavPagerAdapter(
                childFragmentManager
            )
        favBinding.tabsMain.setupWithViewPager(favBinding.viewpagerMain)
        favBinding.tabsMain.getTabAt(0)?.text = getString(R.string.menu_next)
        favBinding.tabsMain.getTabAt(1)?.text = getString(R.string.menu_last)
        favBinding.tabsMain.getTabAt(2)?.text = getString(R.string.menu_team)

        return favBinding.root
    }


}
