package com.haidev.footballmatchapp.menu.match.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.menu.match.models.ResultSearchMatch
import com.haidev.footballmatchapp.menu.match.views.DetailMatchActivity

class ItemSearchMatchViewModel(
    private val context: Context,
    val model: ResultSearchMatch
) {
    var roles: String
    var title: ObservableField<String?> = ObservableField(model.strEvent)
    var date: ObservableField<String?> = ObservableField(model.strDate)
    var scoreAway: ObservableField<String?> = ObservableField(model.intAwayScore)
    var scoreHome: ObservableField<String?> = ObservableField(model.intHomeScore)

    init {
        if (model.intAwayScore == "" && model.intHomeScore == "" || model.intAwayScore == null && model.intHomeScore == null || model.intAwayScore.isEmpty() && model.intHomeScore.isEmpty()) {
            roles = "next"
        } else {
            roles = "last"
        }
    }

    fun clickDetailMatch(view: View) {

        val intent = Intent(context, DetailMatchActivity::class.java)
        intent.putExtra("putIdEvent", model.idEvent)
        intent.putExtra("putTitleEvent", model.strEvent)
        intent.putExtra("putRole", roles)
        context.startActivity(intent)
    }
}