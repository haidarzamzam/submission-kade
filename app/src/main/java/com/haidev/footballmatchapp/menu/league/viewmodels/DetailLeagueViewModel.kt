package com.haidev.footballmatchapp.menu.league.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.footballmatchapp.menu.league.models.DetailLeagueModel
import com.haidev.footballmatchapp.network.ApiObserver
import com.haidev.footballmatchapp.network.RestApi
import com.haidev.footballmatchapp.network.repositories.LeaguesRepository
import com.haidev.footballmatchapp.network.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class DetailLeagueViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listDetailLeagueResponse: MutableLiveData<DetailLeagueModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private lateinit var schedulerProvider: SchedulerProvider
    private lateinit var mainRepository: LeaguesRepository
    private lateinit var apiService: RestApi
    private val compositeDisposable = CompositeDisposable()

    fun setupRepository(scheduler: SchedulerProvider, service: RestApi) {
        schedulerProvider = scheduler
        mainRepository = LeaguesRepository(schedulerProvider)
        apiService = service
    }

    fun requestDetailLeague(id: String?) {
        isLoading.set(true)

        apiService.getDetailLeague(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .subscribe(object : ApiObserver<DetailLeagueModel>(compositeDisposable) {
                override fun onApiSuccess(data: DetailLeagueModel) {
                    isLoading.set(false)
                    listDetailLeagueResponse.postValue(data)
                }

                override fun onApiError(er: Throwable) {
                    isLoading.set(false)
                    error.postValue(er)
                }
            })
    }


    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}