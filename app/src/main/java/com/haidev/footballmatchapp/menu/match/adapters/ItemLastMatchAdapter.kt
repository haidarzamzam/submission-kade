package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListLastMatchBinding
import com.haidev.footballmatchapp.menu.match.models.ResultMatch
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemLastMatchViewModel

class ItemLastMatchAdapter(
    private val context: Context,
    private var listMovie: MutableList<ResultMatch>
) :
    RecyclerView.Adapter<ItemLastMatchAdapter.ItemLastMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemLastMatchViewHolder {
        val binding: ItemListLastMatchBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_last_match,
                parent,
                false
            )
        return ItemLastMatchViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listMovie.size
    }

    override fun onBindViewHolder(holder: ItemLastMatchViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listMovie[fixPosition])
    }

    class ItemLastMatchViewHolder(val binding: ItemListLastMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemLastMatchViewModel

        fun bindBinding(context: Context, model: ResultMatch) {
            viewModel = ItemLastMatchViewModel(
                context,
                model
            )
            binding.itemLastMatch = viewModel
            binding.executePendingBindings()
        }

    }
}