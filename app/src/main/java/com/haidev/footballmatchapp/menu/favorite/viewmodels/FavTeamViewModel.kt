package com.haidev.footballmatchapp.menu.favorite.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class FavTeamViewModel(application: Application) : AndroidViewModel(application)