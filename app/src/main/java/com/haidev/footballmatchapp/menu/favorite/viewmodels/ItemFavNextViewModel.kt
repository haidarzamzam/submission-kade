package com.haidev.footballmatchapp.menu.favorite.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.views.DetailFavActivity

class ItemFavNextViewModel(
    val model: FavMatchModel,
    val context: Context
) {
    var title: ObservableField<String?> = ObservableField(model.title)
    var date: ObservableField<String?> = ObservableField(model.date)
    var scoreAway: ObservableField<String?> = ObservableField(model.scoreAway)
    var scoreHome: ObservableField<String?> = ObservableField(model.scoreHome)

    fun clickDetailFav(view: View) {
        val intent = Intent(context, DetailFavActivity::class.java)
        intent.putExtra(DetailFavActivity.EXTRA_DATA_LIST, model)
        context.startActivity(intent)
    }
}