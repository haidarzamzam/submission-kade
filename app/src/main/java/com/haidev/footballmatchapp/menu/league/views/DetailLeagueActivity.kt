package com.haidev.footballmatchapp.menu.league.views

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivityDetailLeagueBinding
import com.haidev.footballmatchapp.menu.league.models.DetailLeagueModel
import com.haidev.footballmatchapp.menu.league.viewmodels.DetailLeagueViewModel
import com.haidev.footballmatchapp.menu.match.adapters.MyMatchPagerAdapter
import com.haidev.footballmatchapp.menu.match.views.SearchMatchActivity
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import com.squareup.picasso.Picasso

class DetailLeagueActivity : AppCompatActivity() {

    private lateinit var detailLeagueBinding: ActivityDetailLeagueBinding
    private lateinit var vmDetaiLleague: DetailLeagueViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailLeagueBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_league)
        vmDetaiLleague = ViewModelProviders.of(this).get(DetailLeagueViewModel::class.java)
        detailLeagueBinding.detailLeague = vmDetaiLleague
        vmDetaiLleague.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        vmDetaiLleague.listDetailLeagueResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmDetaiLleague.error.observe(this, Observer {
            handlingError(it)
        })

        val id: String? = intent.getStringExtra("putId")
        val title: String? = intent.getStringExtra("putTitle")
        vmDetaiLleague.requestDetailLeague(id)

        detailLeagueBinding.viewpagerMain.adapter =
            MyMatchPagerAdapter(
                supportFragmentManager,
                id
            )
        detailLeagueBinding.tabsMain.setupWithViewPager(detailLeagueBinding.viewpagerMain)
        detailLeagueBinding.tabsMain.getTabAt(0)?.text = getString(R.string.menu_match)
        detailLeagueBinding.tabsMain.getTabAt(1)?.text = getString(R.string.menu_standing)
        detailLeagueBinding.tabsMain.getTabAt(2)?.text = getString(R.string.menu_team)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.action_search, menu)

        val searchView = menu.findItem(R.id.action_search)?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                val intent = Intent(this@DetailLeagueActivity, SearchMatchActivity::class.java)
                intent.putExtra("putQuery", query)
                startActivity(intent)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })


        return super.onCreateOptionsMenu(menu)
    }

    private fun onListDataChange(leagueModel: DetailLeagueModel) {
        detailLeagueBinding.txtTitle.text = leagueModel.leagues[0].strLeague
        Picasso.get().load(leagueModel.leagues[0].strLogo).resize(100, 100)
            .into(detailLeagueBinding.imgLogo)
        detailLeagueBinding.txtDesc.text = leagueModel.leagues[0].strDescriptionEN

        detailLeagueBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(this, throwable?.message, Toast.LENGTH_SHORT).show()
        detailLeagueBinding.progressLoading.isVisible = false
    }
}
