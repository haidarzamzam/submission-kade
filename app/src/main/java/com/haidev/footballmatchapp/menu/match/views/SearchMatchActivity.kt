package com.haidev.footballmatchapp.menu.match.views

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivitySearchMatchBinding
import com.haidev.footballmatchapp.menu.match.adapters.ItemSearchMatchAdapter
import com.haidev.footballmatchapp.menu.match.models.ResultSearchMatch
import com.haidev.footballmatchapp.menu.match.models.SearchMatchModel
import com.haidev.footballmatchapp.menu.match.viewmodels.SearchMatchViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.activity_search_match.*

class SearchMatchActivity : AppCompatActivity() {

    private lateinit var searchEventBinding: ActivitySearchMatchBinding
    private lateinit var vmSearchEvent: SearchMatchViewModel

    private lateinit var adapter: ItemSearchMatchAdapter
    private var listMatch: MutableList<ResultSearchMatch> = mutableListOf()

    var query: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchEventBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_match)
        vmSearchEvent = ViewModelProviders.of(this).get(SearchMatchViewModel::class.java)
        searchEventBinding.searchEvent = vmSearchEvent
        vmSearchEvent.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        query = intent.getStringExtra("putQuery")

        vmSearchEvent.listMatch.observe(this, Observer {
            onListDataChange(it)
        })
        vmSearchEvent.error.observe(this, Observer {
            handlingError(it)
        })


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        vmSearchEvent.requestSearchMatch(query)
        setupRecycler()
        setupSwipeRefresh()
    }

    private fun setupSwipeRefresh() {
        searchEventBinding.swipe.setOnRefreshListener {
            searchEventBinding.swipe.isRefreshing = false
            vmSearchEvent.requestSearchMatch(query)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(this)
        searchEventBinding.rvSearchMatch.layoutManager = lManager
        searchEventBinding.rvSearchMatch.setHasFixedSize(true)

        adapter =
            ItemSearchMatchAdapter(this, listMatch)
        rvSearchMatch.adapter = adapter
    }

    private fun onListDataChange(searchMatchModel: SearchMatchModel?) {
        listMatch.clear()
        if (searchMatchModel?.event == null) {
            Toast.makeText(this, "Search Match NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listMatch.addAll(searchMatchModel.event.filter { it.strSport == "Soccer" })
            rvSearchMatch.post {
                adapter.notifyDataSetChanged()
            }
        }
        searchEventBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(this, throwable?.message, Toast.LENGTH_SHORT).show()
        searchEventBinding.progressLoading.isVisible = false
    }
}
