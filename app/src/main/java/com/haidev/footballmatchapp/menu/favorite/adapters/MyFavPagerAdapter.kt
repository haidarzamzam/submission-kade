package com.haidev.footballmatchapp.menu.favorite.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.haidev.footballmatchapp.menu.favorite.views.FavLastFragment
import com.haidev.footballmatchapp.menu.favorite.views.FavNextFragment
import com.haidev.footballmatchapp.menu.favorite.views.FavTeamFragment

class MyFavPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        FavNextFragment(),
        FavLastFragment(),
        FavTeamFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }


}