package com.haidev.footballmatchapp.menu.match.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.network.ApiObserver
import com.haidev.footballmatchapp.network.RestApi
import com.haidev.footballmatchapp.network.repositories.LeaguesRepository
import com.haidev.footballmatchapp.network.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class DetailTeamViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listDetailTeamResponse: MutableLiveData<TeamModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private lateinit var schedulerProvider: SchedulerProvider
    private lateinit var mainRepository: LeaguesRepository
    private lateinit var apiService: RestApi
    private val compositeDisposable = CompositeDisposable()

    fun setupRepository(scheduler: SchedulerProvider, service: RestApi) {
        schedulerProvider = scheduler
        mainRepository = LeaguesRepository(schedulerProvider)
        apiService = service
    }

    fun requestDetailTeam(id: String?) {
        isLoading.set(true)

        apiService.getDetailTeam(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .subscribe(object : ApiObserver<TeamModel>(compositeDisposable) {
                override fun onApiSuccess(data: TeamModel) {
                    isLoading.set(false)
                    listDetailTeamResponse.postValue(data)
                }

                override fun onApiError(er: Throwable) {
                    isLoading.set(false)
                    error.postValue(er)
                }
            })
    }


    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}