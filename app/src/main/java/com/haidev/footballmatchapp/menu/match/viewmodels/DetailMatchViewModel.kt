package com.haidev.footballmatchapp.menu.match.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.footballmatchapp.menu.match.models.MatchModel
import com.haidev.footballmatchapp.menu.match.models.TeamModel
import com.haidev.footballmatchapp.network.ApiObserver
import com.haidev.footballmatchapp.network.RestApi
import com.haidev.footballmatchapp.network.repositories.LeaguesRepository
import com.haidev.footballmatchapp.network.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class DetailMatchViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listDetailMatchResponse: MutableLiveData<MatchModel> = MutableLiveData()
    var listTeamAwayResponse: MutableLiveData<TeamModel> = MutableLiveData()
    var listTeamHomeResponse: MutableLiveData<TeamModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private lateinit var schedulerProvider: SchedulerProvider
    private lateinit var mainRepository: LeaguesRepository
    private lateinit var apiService: RestApi
    private val compositeDisposable = CompositeDisposable()

    fun setupRepository(scheduler: SchedulerProvider, service: RestApi) {
        schedulerProvider = scheduler
        mainRepository = LeaguesRepository(schedulerProvider)
        apiService = service
    }

    fun requestDetailTeamAway(id: String?) {
        isLoading.set(true)

        apiService.getDetailTeam(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .subscribe(object : ApiObserver<TeamModel>(compositeDisposable) {
                override fun onApiSuccess(data: TeamModel) {
                    isLoading.set(false)
                    listTeamAwayResponse.postValue(data)
                }

                override fun onApiError(er: Throwable) {
                    isLoading.set(false)
                    error.postValue(er)
                }
            })
    }

    fun requestDetailTeamHome(id: String?) {
        isLoading.set(true)
        apiService.getDetailTeam(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .subscribe(object : ApiObserver<TeamModel>(compositeDisposable) {
                override fun onApiSuccess(data: TeamModel) {
                    isLoading.set(false)
                    listTeamHomeResponse.postValue(data)
                }

                override fun onApiError(er: Throwable) {
                    isLoading.set(false)
                    error.postValue(er)
                }
            })
    }

    fun requestDetailMatch(id: String?) {
        isLoading.set(true)

        apiService.getDetailMatch(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .subscribe(object : ApiObserver<MatchModel>(compositeDisposable) {
                override fun onApiSuccess(data: MatchModel) {
                    isLoading.set(false)
                    listDetailMatchResponse.postValue(data)
                }

                override fun onApiError(er: Throwable) {
                    isLoading.set(false)
                    error.postValue(er)
                }
            })
    }


    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}