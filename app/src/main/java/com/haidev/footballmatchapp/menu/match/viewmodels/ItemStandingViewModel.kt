package com.haidev.footballmatchapp.menu.match.viewmodels

import android.content.Context
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.menu.match.models.ResultStanding

class ItemStandingViewModel(
    private val context: Context,
    val model: ResultStanding
) {
    var name: ObservableField<String?> = ObservableField(model.name)
    var win: ObservableField<String?> = ObservableField(model.win)
    var draw: ObservableField<String?> = ObservableField(model.draw)
    var loss: ObservableField<String?> = ObservableField(model.loss)
    var total: ObservableField<String?> = ObservableField(model.total)

}