package com.haidev.footballmatchapp.menu.match.viewmodels

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import com.haidev.footballmatchapp.databinding.ItemListLeagueBinding
import com.haidev.footballmatchapp.menu.league.models.Country
import com.haidev.footballmatchapp.menu.league.views.DetailLeagueActivity
import com.squareup.picasso.Picasso

class ItemLeagueViewModel(
    private val context: Context,
    val model: Country,
    val binding: ItemListLeagueBinding
) {

    var title: ObservableField<String?> = ObservableField(model.strLeague)
    var desc: ObservableField<String?> = ObservableField(model.strLeagueAlternate)

    init {
        Picasso.get().load(
            model.strBadge
        ).into(binding.ivLeague)
    }

    fun clickDetailleague(view: View) {
        val intent = Intent(context, DetailLeagueActivity::class.java)
        intent.putExtra("putId", model.idLeague)
        intent.putExtra("putTitle", model.strLeague)
        context.startActivity(intent)
    }
}