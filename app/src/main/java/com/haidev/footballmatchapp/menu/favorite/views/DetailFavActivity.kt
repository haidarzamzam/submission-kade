package com.haidev.footballmatchapp.menu.favorite.views

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ActivityDetailFavBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.DetailFavViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailFavActivity : AppCompatActivity() {

    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    private lateinit var detailFavBinding: ActivityDetailFavBinding
    private lateinit var vmDetailFav: DetailFavViewModel

    private lateinit var listFavModel: FavMatchModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailFavBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_fav)
        vmDetailFav = ViewModelProviders.of(this).get(DetailFavViewModel::class.java)
        detailFavBinding.detailFav = vmDetailFav

        listFavModel = intent.getParcelableExtra(EXTRA_DATA_LIST)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = listFavModel.title

        detailFavBinding.txtTitle.text = listFavModel.title
        detailFavBinding.txtDate.text = listFavModel.date
        detailFavBinding.txtDesc.text = listFavModel.desc
        detailFavBinding.txtScoreAway.text = listFavModel.scoreAway
        detailFavBinding.txtScoreHome.text = listFavModel.scoreHome
        detailFavBinding.txtHomeGoals.text = listFavModel.goalsHome
        detailFavBinding.txtAwayGoals.text = listFavModel.goalsAway
        detailFavBinding.txtRedCardAway.text = listFavModel.redCardAway
        detailFavBinding.txtRedCardHome.text = listFavModel.redCardHome
        detailFavBinding.txtYellowCardAways.text = listFavModel.yellowCardAway
        detailFavBinding.txtYellowCardHome.text = listFavModel.yellowCardHome

        favMovieDbState()
    }

    private fun favMovieDbState() {

        database.use {
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ID_FAVORITE = {id})",
                    "id" to listFavModel.id.toString()
                )
            val favorite = result.parseList(classParser<FavMatchModel>())
            if (favorite.isNotEmpty()) isFavorite = true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            if (isFavorite) removeFromFavMovieDb() else addToFavMovieDb()

            isFavorite = !isFavorite
            setFavMovieDb()

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addToFavMovieDb() {
        try {
            database.use {
                insert(
                    FavMatchModel.TABLE_FAVORITE,
                    FavMatchModel.ID_FAVORITE to listFavModel.id,
                    FavMatchModel.TITLE_FAVORITE to listFavModel.title,
                    FavMatchModel.DATE_FAVORITE to listFavModel.date,
                    FavMatchModel.DESC_FAVORITE to listFavModel.desc,
                    FavMatchModel.SCORE_AWAY to listFavModel.scoreAway,
                    FavMatchModel.SCORE_HOME to listFavModel.scoreHome,
                    FavMatchModel.GOALS_AWAY to listFavModel.goalsAway,
                    FavMatchModel.GOALS_HOME to listFavModel.goalsHome,
                    FavMatchModel.RED_CARD_AWAY to listFavModel.redCardAway,
                    FavMatchModel.RED_CARD_HOME to listFavModel.redCardHome,
                    FavMatchModel.YELLOW_CARD_AWAY to listFavModel.yellowCardAway,
                    FavMatchModel.YELLOW_CARD_HOME to listFavModel.yellowCardHome,
                    FavMatchModel.ROLE_FAVORITE to listFavModel.role
                )

            }

            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavMovieDb() {
        try {
            database.use {
                delete(
                    FavMatchModel.TABLE_FAVORITE,
                    "(ID_FAVORITE = {id})",
                    "id" to listFavModel.id.toString()
                )
            }
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, "Removed to favorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavMovieDb() {
        if (isFavorite)
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp)
        else
            menuItem?.findItem(R.id.action_favorite)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white_24dp)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.action_fav, menu)
        menuItem = menu
        setFavMovieDb()
        return true
    }
}
