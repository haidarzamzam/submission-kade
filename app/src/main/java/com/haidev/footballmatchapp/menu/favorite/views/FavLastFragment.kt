package com.haidev.footballmatchapp.menu.favorite.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentFavLastBinding
import com.haidev.footballmatchapp.db.database
import com.haidev.footballmatchapp.menu.favorite.adapters.ItemFavLastAdapter
import com.haidev.footballmatchapp.menu.favorite.models.FavMatchModel
import com.haidev.footballmatchapp.menu.favorite.viewmodels.FavLastViewModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavLastFragment : Fragment() {

    private lateinit var favLastBinding: FragmentFavLastBinding
    private lateinit var vmFavLast: FavLastViewModel

    private var isExist: Boolean = false
    private lateinit var adapter: ItemFavLastAdapter
    private var listFavLastMatch: MutableList<FavMatchModel> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favLastBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_fav_last, container, false)
        vmFavLast = ViewModelProviders.of(this).get(FavLastViewModel::class.java)
        favLastBinding.favLast = vmFavLast

        favMatchDbState()
        setupRecycler()
        return favLastBinding.root
    }

    override fun onResume() {
        super.onResume()
        favMatchDbState()
        setupRecycler()
    }

    private fun favMatchDbState() {
        context?.database?.use {
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ROLE_FAVORITE = {id})",
                    "id" to "last"
                )
            val datas = result.parseList(classParser<FavMatchModel>())
            if (datas.isNotEmpty()) isExist = true
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        favLastBinding.rvFavLast.layoutManager = lManager
        favLastBinding.rvFavLast.setHasFixedSize(true)

        adapter = ItemFavLastAdapter(context!!, listFavLastMatch)

        favLastBinding.rvFavLast.adapter = adapter
        if (isExist) {
            favLastBinding.txtNoDataMatch.visibility = View.INVISIBLE
            showFavorite()
        } else {
            favLastBinding.txtNoDataMatch.visibility = View.VISIBLE
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            listFavLastMatch.clear()
            val result = select(FavMatchModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ROLE_FAVORITE = {id})",
                    "id" to "last"
                )

            val favorite = result.parseList(classParser<FavMatchModel>())
            listFavLastMatch.addAll(favorite)
            adapter.notifyDataSetChanged()

        }
    }
}
