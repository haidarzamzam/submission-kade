package com.haidev.footballmatchapp.menu.match.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.FragmentMatchBinding
import com.haidev.footballmatchapp.menu.match.adapters.ItemLastMatchAdapter
import com.haidev.footballmatchapp.menu.match.adapters.ItemNextMatchAdapter
import com.haidev.footballmatchapp.menu.match.models.MatchModel
import com.haidev.footballmatchapp.menu.match.models.ResultMatch
import com.haidev.footballmatchapp.menu.match.viewmodels.MatchViewModel
import com.haidev.footballmatchapp.network.ServiceFactory
import com.haidev.footballmatchapp.network.scheduler.AppSchedulerProvider
import kotlinx.android.synthetic.main.fragment_match.*


class MatchFragment(val id: String?) : Fragment() {

    private lateinit var lastMatchBinding: FragmentMatchBinding
    private lateinit var vmMatch: MatchViewModel

    private lateinit var adapterLast: ItemLastMatchAdapter
    private lateinit var adapterNext: ItemNextMatchAdapter
    private var listLastMatch: MutableList<ResultMatch> = mutableListOf()
    private var listNextMatch: MutableList<ResultMatch> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        lastMatchBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_match, container, false)
        vmMatch = ViewModelProviders.of(this).get(MatchViewModel::class.java)
        lastMatchBinding.match = vmMatch
        vmMatch.setupRepository(AppSchedulerProvider(), ServiceFactory.create())

        vmMatch.listLastMatch.observe(this, Observer {
            onListDataChangeLast(it)
        })

        vmMatch.listNextMatch.observe(this, Observer {
            onListDataChangeNext(it)
        })

        vmMatch.error.observe(this, Observer {
            handlingError(it)
        })

        vmMatch.requestLastMatch(id)
        vmMatch.requestNextMatch(id)

        setupRecyclerLast()
        setupRecyclerNext()
        setupSwipeRefresh()
        return lastMatchBinding.root
    }

    private fun setupSwipeRefresh() {
        lastMatchBinding.swipe.setOnRefreshListener {
            lastMatchBinding.swipe.isRefreshing = false
            vmMatch.requestLastMatch(id)
        }
    }

    private fun setupRecyclerNext() {
        val lManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        lastMatchBinding.rvNextMatch.layoutManager = lManager
        lastMatchBinding.rvNextMatch.setHasFixedSize(true)

        adapterNext = ItemNextMatchAdapter(
            context!!,
            listNextMatch
        )
        lastMatchBinding.rvNextMatch.adapter = adapterNext
    }

    private fun setupRecyclerLast() {
        val lManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        lastMatchBinding.rvLastMatch.layoutManager = lManager
        lastMatchBinding.rvLastMatch.setHasFixedSize(true)

        adapterLast = ItemLastMatchAdapter(
            context!!,
            listLastMatch
        )
        lastMatchBinding.rvLastMatch.adapter = adapterLast

    }

    private fun onListDataChangeNext(matchModel: MatchModel?) {
        listNextMatch.clear()
        if (matchModel?.events == null) {
            Toast.makeText(context, "Next Match NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listNextMatch.addAll(matchModel.events)
            rvNextMatch.post {
                adapterNext.notifyDataSetChanged()
            }
        }
        lastMatchBinding.progressLoading.isVisible = false
    }

    private fun onListDataChangeLast(matchModel: MatchModel?) {
        listLastMatch.clear()
        if (matchModel?.events == null) {
            Toast.makeText(context, "Last Match NULL!!", Toast.LENGTH_SHORT).show()
        } else {
            listLastMatch.addAll(matchModel.events)
            rvLastMatch.post {
                adapterLast.notifyDataSetChanged()
            }
        }
        lastMatchBinding.progressLoading.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        lastMatchBinding.progressLoading.isVisible = false
    }

}
