package com.haidev.footballmatchapp.menu.league.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListLeagueBinding
import com.haidev.footballmatchapp.menu.league.models.Country
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemLeagueViewModel

class ItemLeagueAdapter(private val context: Context, private var listMovie: MutableList<Country>) :
    RecyclerView.Adapter<ItemLeagueAdapter.ItemListLeagueViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemListLeagueViewHolder {
        val binding: ItemListLeagueBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_league,
                parent,
                false
            )
        return ItemListLeagueViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listMovie.size
    }

    override fun onBindViewHolder(holder: ItemListLeagueViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listMovie[fixPosition])
    }

    class ItemListLeagueViewHolder(val binding: ItemListLeagueBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemLeagueViewModel

        fun bindBinding(context: Context, model: Country) {
            viewModel = ItemLeagueViewModel(
                context,
                model,
                binding
            )
            binding.itemLeague = viewModel
            binding.executePendingBindings()
        }

    }
}