package com.haidev.footballmatchapp.menu.match.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.haidev.footballmatchapp.R
import com.haidev.footballmatchapp.databinding.ItemListSearchMatchBinding
import com.haidev.footballmatchapp.menu.match.models.ResultSearchMatch
import com.haidev.footballmatchapp.menu.match.viewmodels.ItemSearchMatchViewModel

class ItemSearchMatchAdapter(
    private val context: Context,
    private var listMatch: MutableList<ResultSearchMatch>
) :
    RecyclerView.Adapter<ItemSearchMatchAdapter.ItemSearchMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemSearchMatchViewHolder {
        val binding: ItemListSearchMatchBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_list_search_match,
                parent,
                false
            )
        return ItemSearchMatchViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listMatch.size
    }

    override fun onBindViewHolder(holder: ItemSearchMatchViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.bindBinding(context, listMatch[fixPosition])
    }

    class ItemSearchMatchViewHolder(val binding: ItemListSearchMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var viewModel: ItemSearchMatchViewModel

        fun bindBinding(context: Context, model: ResultSearchMatch) {
            viewModel = ItemSearchMatchViewModel(
                context,
                model
            )
            binding.itemSeachMatch = viewModel
            binding.executePendingBindings()
        }

    }
}