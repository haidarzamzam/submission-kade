package com.haidev.footballmatchapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.haidev.footballmatchapp.menu.match.viewmodels.SearchMatchViewModel
import com.haidev.footballmatchapp.network.RestApi
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

open class SearchMatchViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var searchMathViewModel: SearchMatchViewModel

    @RelaxedMockK
    lateinit var apiServices: RestApi

    private var application = mockk<Application>()
    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        searchMathViewModel = spyk(SearchMatchViewModel(application))
        searchMathViewModel.setupRepository(testSchedulerProvider, apiServices)
    }

    @Test
    fun fetchTeam_success() {
        val query = "liverpool"
        val mockResponse = MockProvider.responseSearchMatch()

        every { apiServices.getSearchMatch(query) } returns Observable.just(mockResponse)

        searchMathViewModel.requestSearchMatch(query)
        testScheduler.triggerActions()

        searchMathViewModel.isLoading.get() shouldEqual false
        searchMathViewModel.listMatch.value shouldEqual mockResponse
        verify { apiServices.getSearchMatch(any()) }
    }

    @Test
    fun fetchTeam_error() {
        val query = "liverpool"
        val mockResponse = Exception("Error")

        every { apiServices.getSearchMatch(query) } returns Observable.error(mockResponse)

        searchMathViewModel.requestSearchMatch(query)
        testScheduler.triggerActions()

        searchMathViewModel.isLoading.get() shouldEqual false
        verify { apiServices.getSearchMatch(any()) }
    }
}

