package com.haidev.footballmatchapp

import com.haidev.footballmatchapp.network.scheduler.SchedulerProvider
import io.reactivex.schedulers.TestScheduler

/**
 * Created by Yoga C. Pranata on 2019-12-11.
 * Android Engineer
 */
class TestSchedulerProvider(private val testScheduler: TestScheduler) : SchedulerProvider {
    override fun io() = testScheduler
    override fun computation() = testScheduler
    override fun mainThread() = testScheduler
}