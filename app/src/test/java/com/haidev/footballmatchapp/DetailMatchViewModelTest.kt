package com.haidev.footballmatchapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.haidev.footballmatchapp.menu.match.viewmodels.DetailMatchViewModel
import com.haidev.footballmatchapp.network.RestApi
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailMatchViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var detailMathViewModel: DetailMatchViewModel

    @RelaxedMockK
    lateinit var apiServices: RestApi

    private var application = mockk<Application>()
    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        detailMathViewModel = spyk(DetailMatchViewModel(application))
        detailMathViewModel.setupRepository(testSchedulerProvider, apiServices)
    }

    @Test
    fun fetchTeam_success() {
        val id = "4387"
        val mockResponse = MockProvider.responseMatch()

        every { apiServices.getDetailMatch(id) } returns Observable.just(mockResponse)

        detailMathViewModel.requestDetailMatch(id)
        testScheduler.triggerActions()

        detailMathViewModel.isLoading.get() shouldEqual false
        detailMathViewModel.listDetailMatchResponse.value shouldEqual mockResponse
        verify { apiServices.getDetailMatch(any()) }
    }

    @Test
    fun fetchTeam_error() {
        val id = "4387"
        val mockResponse = Exception("Error")

        every { apiServices.getDetailMatch(id) } returns Observable.error(mockResponse)

        detailMathViewModel.requestDetailMatch(id)
        testScheduler.triggerActions()

        detailMathViewModel.isLoading.get() shouldEqual false
        verify { apiServices.getDetailMatch(any()) }
    }
}