package com.haidev.footballmatchapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.haidev.footballmatchapp.menu.match.viewmodels.MatchViewModel
import com.haidev.footballmatchapp.network.RestApi
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LastMatchViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var lastMathViewModel: MatchViewModel

    @RelaxedMockK
    lateinit var apiServices: RestApi

    private var application = mockk<Application>()
    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        lastMathViewModel = spyk(MatchViewModel(application))
        lastMathViewModel.setupRepository(testSchedulerProvider, apiServices)
    }

    @Test
    fun fetchTeam_success() {
        val id = "4387"
        val mockResponse = MockProvider.responseMatch()

        every { apiServices.getLastMatch(id) } returns Observable.just(mockResponse)

        lastMathViewModel.requestLastMatch(id)
        testScheduler.triggerActions()

        lastMathViewModel.isLoading.get() shouldEqual false
        lastMathViewModel.listLastMatch.value shouldEqual mockResponse
        verify { apiServices.getLastMatch(any()) }
    }

    @Test
    fun fetchTeam_error() {
        val id = "4387"
        val mockResponse = Exception("Error")

        every { apiServices.getLastMatch(id) } returns Observable.error(mockResponse)

        lastMathViewModel.requestLastMatch(id)
        testScheduler.triggerActions()

        lastMathViewModel.isLoading.get() shouldEqual false
        verify { apiServices.getLastMatch(any()) }
    }
}