package com.haidev.footballmatchapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.haidev.footballmatchapp.menu.match.viewmodels.MatchViewModel
import com.haidev.footballmatchapp.network.RestApi
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class NextMatchViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var nextMathViewModel: MatchViewModel

    @RelaxedMockK
    lateinit var apiServices: RestApi

    private var application = mockk<Application>()
    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        nextMathViewModel = spyk(MatchViewModel(application))
        nextMathViewModel.setupRepository(testSchedulerProvider, apiServices)
    }

    @Test
    fun fetchTeam_success() {
        val id = "4387"
        val mockResponse = MockProvider.responseMatch()

        every { apiServices.getNextMatch(id) } returns Observable.just(mockResponse)

        nextMathViewModel.requestNextMatch(id)
        testScheduler.triggerActions()

        nextMathViewModel.isLoading.get() shouldEqual false
        nextMathViewModel.listNextMatch.value shouldEqual mockResponse
        verify { apiServices.getNextMatch(any()) }
    }

    @Test
    fun fetchTeam_error() {
        val id = "4387"
        val mockResponse = Exception("Error")

        every { apiServices.getNextMatch(id) } returns Observable.error(mockResponse)

        nextMathViewModel.requestNextMatch(id)
        testScheduler.triggerActions()

        nextMathViewModel.isLoading.get() shouldEqual false
        verify { apiServices.getNextMatch(any()) }
    }
}