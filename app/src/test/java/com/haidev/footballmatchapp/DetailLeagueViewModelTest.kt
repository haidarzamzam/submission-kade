package com.haidev.footballmatchapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.haidev.footballmatchapp.menu.league.viewmodels.DetailLeagueViewModel
import com.haidev.footballmatchapp.network.RestApi
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailLeagueViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var detailLeagueViewModel: DetailLeagueViewModel

    @RelaxedMockK
    lateinit var apiServices: RestApi

    private var application = mockk<Application>()
    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        detailLeagueViewModel = spyk(DetailLeagueViewModel(application))
        detailLeagueViewModel.setupRepository(testSchedulerProvider, apiServices)
    }

    @Test
    fun fetchTeam_success() {
        val id = "4328"
        val mockResponse = MockProvider.responseDetailLeague()

        every { apiServices.getDetailLeague(id) } returns Observable.just(mockResponse)

        detailLeagueViewModel.requestDetailLeague(id)
        testScheduler.triggerActions()

        detailLeagueViewModel.isLoading.get() shouldEqual false
        detailLeagueViewModel.listDetailLeagueResponse.value shouldEqual mockResponse
        verify { apiServices.getDetailLeague(any()) }
    }

    @Test
    fun fetchTeam_error() {
        val id = "4328"
        val mockResponse = Exception("Error")

        every { apiServices.getDetailLeague(id) } returns Observable.error(mockResponse)

        detailLeagueViewModel.requestDetailLeague(id)
        testScheduler.triggerActions()

        detailLeagueViewModel.isLoading.get() shouldEqual false
        verify { apiServices.getDetailLeague(any()) }
    }
}